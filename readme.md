# SteamKit2

SteamKit2 is a .NET library designed to interoperate with Valve's [Steam network](http://store.steampowered.com/about). It aims to provide a simple, yet extensible, interface to perform various actions on the network.


## Description

SteamKit2 is a .NET library designed to interoperate with Valve's Steam network. Please note that this is a modified version of the original one being maintained on [GitHub](https://github.com/SteamRE/SteamKit) in order to be compatible with iOS platform and this repository is for archiving purpose only.


## License

SteamKit2 is released under the [LGPL-2.1 license](http://www.tldrlegal.com/license/gnu-lesser-general-public-license-v2.1-%28lgpl-2.1%29).
