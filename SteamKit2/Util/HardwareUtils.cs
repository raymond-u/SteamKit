﻿using System;
using System.IO;
using System.Text;

namespace SteamKit2
{
    abstract class MachineInfoProvider
    {
        public abstract byte[] GetMachineGuid( string uniqueIdentifier );
        public abstract byte[] GetMacAddress( string uniqueIdentifier );
        public abstract byte[] GetDiskId( string uniqueIdentifier );
    }

    class PortableInfoProvider : MachineInfoProvider
    {
        public override byte[] GetMachineGuid( string uniqueIdentifier )
        {
            return Encoding.UTF8.GetBytes( "Steamulate-MachineGuid-" + uniqueIdentifier );
        }

        public override byte[] GetMacAddress( string uniqueIdentifier )
        {
            return Encoding.UTF8.GetBytes( "Steamulate-MacAddress-" + uniqueIdentifier );
        }

        public override byte[] GetDiskId( string uniqueIdentifier )
        {
            return Encoding.UTF8.GetBytes( "Steamulate-DiskId-" + uniqueIdentifier );
        }
    }

    static class HardwareUtils
    {
        class MachineID : MessageObject
        {
            public MachineID()
            {
                this.KeyValues["BB3"] = new KeyValue();
                this.KeyValues["FF2"] = new KeyValue();
                this.KeyValues["3B3"] = new KeyValue();
            }


            public void SetBB3( string value )
            {
                this.KeyValues["BB3"].Value = value;
            }

            public void SetFF2( string value )
            {
                this.KeyValues["FF2"].Value = value;
            }

            public void Set3B3( string value )
            {
                this.KeyValues["3B3"].Value = value;
            }

            public void Set333( string value )
            {
                this.KeyValues["333"] = new KeyValue( value: value );
            }
        }

        public static byte[] GetMachineID( string uniqueIdentifier = "0000000000" )
        {
            MachineID machineId = GenerateMachineID( uniqueIdentifier );

            using ( MemoryStream ms = new MemoryStream() )
            {
                machineId.WriteToStream( ms );

                return ms.ToArray();
            }
        }


        static MachineID GenerateMachineID( string uniqueIdentifier )
        {
            // the aug 25th 2015 CM update made well-formed machine MessageObjects required for logon
            // this was flipped off shortly after the update rolled out, likely due to linux steamclients running on distros without a way to build a machineid
            // so while a valid MO isn't currently (as of aug 25th) required, they could be in the future and we'll abide by The Valve Law now

            var machineId = new MachineID();

            MachineInfoProvider provider = new PortableInfoProvider();

            machineId.SetBB3( GetHexString( provider.GetMachineGuid( uniqueIdentifier ) ) );
            machineId.SetFF2( GetHexString( provider.GetMacAddress( uniqueIdentifier ) ) );
            machineId.Set3B3( GetHexString( provider.GetDiskId( uniqueIdentifier ) ) );

            // 333 is some sort of user supplied data and is currently unused

            return machineId;
        }

        static string GetHexString( byte[] data )
        {
            data = CryptoHelper.SHAHash( data );

            return BitConverter.ToString( data )
                .Replace( "-", "" )
                .ToLower();
        }
    }
}
